# Computer Vision Toolbox

## Members
* Gherardi Alessandro 817084
* Simone Giuseppe Locatelli 816781

## Project
Development of a desktop application in Python for comparing models for image classification and carrying out preprocessing (e.g. data augmentation) on the data, to be used for training or just for saving the processed images.

## Architectural Spike
![ArchitecturalSpike](images/ArchitecturalSpike.png)

The application consists of 2 main blocks:
* A Machine Learning backend implemented in [TensorFlow](https://www.tensorflow.org/)
* A frontend that allows interaction with the application by exploiting the backend that will be implemented with [PySimpleGUI](https://pysimplegui.readthedocs.io/en/latest/)

## Interface architecture
It was decided to implement a single-screen interface, according to the architecture shown below.
![GUI](images/GUI.png)

## Application Start-up
To start the application, it is first necessary to ensure that a version of Python3 is installed on your system.
Then install the necessary libraries using the command:
```
pip3 install -r requirements.txt
```
Once installed, it will be possible to start the application with:
```
python3 run.py
```
