import os
import threading
import numpy as np
import tensorflow as tf
from keras import layers, callbacks
from tensorflow import keras
from keras.applications.resnet import preprocess_input as resnet_preprocess
from keras.applications.mobilenet_v2 import preprocess_input as mobilenet_preprocess
import matplotlib.pyplot as plt
from src import sg
from sklearn.metrics import confusion_matrix
import seaborn as sns

loaded_model = None
train_ds, val_ds = None, None


def load(model):
    """ Initialise selected ML model

        Parameters
        ----------
        model: str
            Name of the model to be loaded

        """
    global loaded_model
    try:
        loaded_model = tf.keras.models.load_model('./models/' + model)
    except Exception as e:
        loaded_model = None


def save_model(model, t_accuracy, t_loss, v_accuracy, v_loss):
    """ Window for saving the model

    Parameters
    ----------
    model: Any
        keras model object
    t_accuracy: float
        training accuracy
    t_loss: float
        training loss
    v_accuracy: float
        validation accuracy
    v_loss: float
        validation loss

    """
    from src import sg
    path = ""
    name = ""
    final_path = ""
    layout = [
        [sg.Text("Chose a folder and a new folder name for save the model,")],
        [sg.Text("or close if you don't want to save the model")],
        [sg.Text("Training finished.\nTrain Accuracy: {}%\nTrain Loss: {}\nValidation Accuracy:{}%\nValidation "
                 "Loss: {}".format(t_accuracy, t_loss, v_accuracy, v_loss))],
        [sg.Text("Folder: "), sg.Input(size=(40, 4), disabled=True, enable_events=True, text_color="black",
                                       key="-BROWSE-"),
         sg.FolderBrowse()],
        [sg.Text("Name: "), sg.Input(size=(40, 4), key="-NAME-"), sg.Button("Ok")],
        [sg.Button("Save", disabled=True), sg.Button("Close")]
    ]

    save_win = sg.Window(title="Save Model",
                         finalize=True,
                         layout=layout)
    save_win.bring_to_front()

    while True:
        event, value = save_win.read()
        if event == sg.WIN_CLOSED or event == "Close":
            break
        if event == "-BROWSE-" and value["-BROWSE-"] != '':
            path = value["-BROWSE-"]
        if event == "Ok" and value["-NAME-"] != '':
            name = value["-NAME-"]
        if name != '' and path != '':
            final_path = path + "/" + name
            save_win["Save"].update(disabled=False)
        if event == "Save":
            model.save(final_path)
            break

    save_win.close()


def instantiate_model(model):
    """ Load the selected ML model by showing a popup during loading

        Parameters
        ----------
        model: str
            Name of the model to be loaded

        Returns
        ----------
        loaded_model
            The tensorflow model
        """
    global loaded_model
    from src import sg

    thread = None
    while True:
        if not thread:
            thread = threading.Thread(target=load, args=(model,), daemon=True)
            thread.start()
        if thread:
            sg.popup_animated(sg.DEFAULT_BASE64_LOADING_GIF,
                              message="Wait, we are loading the model...",
                              no_titlebar=True, time_between_frames=50, text_color='black', background_color='white')
            thread.join(0)
            if not thread.is_alive():
                thread = None
                sg.popup_animated(None)
                break

    return loaded_model


def train_eval_model(model_name, model, dataset, train_split, epochs, batch_size, optimizer, lr, loss, dropout):
    """ Train the chosen model on the selected data

            Parameters
            ----------
            model_name: str
                The name of the model to be trained
            model
                The model to be trained
            dataset: str
                The path to dataset
            train_split: float
                The value of the split for the dataset
            epochs: int
                The epochs needed to train the model
            batch_size: int
                The number of images per batch
            optimizer: str
                The name of the optimizer to employ
            lr: float
                The learning rate used during training
            loss: str
                The name of the loss to employ during training
            dropout: bool
                Flag that indicates if a Dropout will be used during training

            Returns
            ----------
            tuple
                train accuracy, train loss, validation accuracy, validation loss
            """

    class CustomCallback(keras.callbacks.Callback):
        def on_epoch_begin(self, epoch, logs=None):
            window.bring_to_front()
            window['-PROGBAR-'].update(epoch+1)
            window['-EPOCH-'].update(str(epoch+2))

    layout = [
        [sg.Text("During training don't close this window")],
        [sg.ProgressBar(epochs, size=(epochs, 4), key='-PROGBAR-'), sg.Text("1", key='-EPOCH-'),
         sg.Text("/"), sg.Text(str(epochs))]
    ]

    window = sg.Window(title="Training",
                       layout=layout,
                       finalize=True)

    IMG_SIZE = (224, 224)
    NUM_CLASSES = len(os.listdir(dataset))
    opt = None
    if optimizer == "Adam":
        opt = keras.optimizers.Adam(learning_rate=lr)
    elif optimizer == "SGD":
        opt = keras.optimizers.SGD(learning_rate=lr)
    elif optimizer == "RMSprop":
        opt = keras.optimizers.RMSprop(learning_rate=lr)

    PRE_PROCESSING = resnet_preprocess if model_name == "ResNet-101" else mobilenet_preprocess

    global train_ds
    global val_ds
    train_ds = tf.keras.utils.image_dataset_from_directory(
        dataset,
        validation_split=1 - train_split,
        subset="training",
        seed=42,
        image_size=IMG_SIZE,
        batch_size=batch_size)

    val_ds = tf.keras.utils.image_dataset_from_directory(
        dataset,
        validation_split=1 - train_split,
        subset="validation",
        seed=42,
        image_size=IMG_SIZE,
        batch_size=batch_size)

    normalization_layer = tf.keras.layers.Rescaling(1. / 255)
    train_ds = train_ds.map(lambda x, y: (normalization_layer(x), y))
    val_ds = val_ds.map(lambda x, y: (normalization_layer(x), y))

    train_ds = train_ds.map(lambda x, y: (PRE_PROCESSING(x), y))
    val_ds = val_ds.map(lambda x, y: (PRE_PROCESSING(x), y))

    model.trainable = False

    ft_model = keras.models.Sequential()
    ft_model.add(model)
    ft_model.add(layers.GlobalAveragePooling2D())
    ft_model.add(layers.Dense(256, activation="relu"))
    if dropout:
        ft_model.add(layers.Dropout(0.4))
    ft_model.add(layers.Dense(NUM_CLASSES, activation="softmax"))

    try:
        ft_model.compile(optimizer=opt, metrics=['accuracy'], loss=loss)

        history = ft_model.fit(
            train_ds,
            validation_data=val_ds,
            epochs=epochs,
            callbacks=[CustomCallback()]
        )
    except Exception as e:
        sg.PopupError(f"Error: {e}")
        return None, None, None, None, None, None

    train_acc = np.round(np.max(history.history['accuracy']) * 100, 2)
    train_loss = np.round(np.min(history.history['loss']), 2)

    val_loss, val_acc = ft_model.evaluate(val_ds)

    val_loss = np.round(val_loss, 2)
    val_acc = np.round(val_acc * 100, 2)

    window.close()

    save_model(ft_model, train_acc, train_loss, val_acc, val_loss)
    return ft_model, history, train_acc, train_loss, val_acc, val_loss


def plot_history(history):
    """ Plot the acc/loss trends

    Parameters
    ----------
    history: Any
        A history object that contains all information collected during training.

    Returns
    -------
    fig: Any
        The current figure

    """
    x_plot = list(range(1, len(history.history['val_accuracy']) + 1))
    plt.figure(1)

    plt.subplot(2, 1, 1)
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')
    plt.plot(x_plot, history.history['accuracy'])
    plt.plot(x_plot, history.history['val_accuracy'])
    plt.legend(['Training', 'Validation'], loc='lower right')
    plt.grid()

    plt.subplot(2, 1, 2)
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.plot(x_plot, history.history['loss'])
    plt.plot(x_plot, history.history['val_loss'])
    plt.legend(['Training', 'Validation'])
    plt.grid()

    fig = plt.gcf()

    return fig


def plot_cm(model):
    """ Plot the acc/loss trends

        Parameters
        ----------
        model: Any
            The current trained model.

        Returns
        -------
        fig: Any
            The current figure

        """
    global val_ds
    # print("model: ", model)
    predictions = np.argmax(model.predict(val_ds), axis=1)
    labels = []
    for _, y in val_ds:
        labels = np.concatenate([labels, y.numpy().astype(int)])

    cm = confusion_matrix(labels, predictions)
    plt.figure(1)
    sns.heatmap(cm, annot=True, fmt="d")
    plt.title('Confusion matrix')
    plt.ylabel('Actual label')
    plt.xlabel('Predicted label')

    print('True Negatives: ', cm[0][0])
    print('False Positives: ', cm[0][1])
    print('False Negatives: ', cm[1][0])
    print('True Positives: ', cm[1][1])

    fig = plt.gcf()

    return fig
