import PySimpleGUI as sg
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from .util import *


def add_history_layout(num):
    """ Add frames to history layout dinamically

    Parameters
    ----------
    num: int
        Number that identify the model to save

    Returns
    -------
    layout: list(list())
        Layout to add to main window

    """
    key_txt = '-MODEL' + str(num) + 'NAME-'
    param_txt = '-MODEL' + str(num) + 'PARAM-'
    key_button = '-LOAD' + str(num) + '-'
    name = "model" + str(num)
    model_layout = [
        [sg.Column([
            [sg.Text("", key=key_txt)],
            [sg.Text("", key=param_txt)]], size=(350, 80)),
            sg.Button("Load", disabled=True, key=key_button, size=(50, 25))]]
    layout = [[sg.Frame(name, size=(450, 90), layout=model_layout, element_justification="center")]]
    return layout


def check_if_model_already_saved(model_dic, params, num):
    """ Check if a model with the specified parameters is present.

    Parameters
    ----------
    model_dic: dict
        Dictionary that contains saved models parameters
    params: dict
        Dictionary that contains current parameters
    num: int
        Number that identify the model to save

    Returns
    -------
    b: bool
        True if new model isn't saved, False otherwise

    """
    for i in range(1, num):
        model_name = "model"+str(i)
        dict1 = model_dic[model_name].copy()
        dict2 = params.copy()
        dict1.pop("model_loaded")
        dict2.pop("model_loaded")
        if dict1 == dict2:
            return False
    return True


def dataset_in():
    """ Handle the addition of a new dataset.

        Returns
        -------
        data: str
            The path of the selected dataset
        """
    data = None
    column_layout = [
        [sg.Text("Select folder containing the dataset.", text_color="orange")],
        [sg.Text("Only images in JPG BMP e PNG format will be accepted.", text_color="orange")],
        [sg.Text("The maximum size for the dataset is 500MB.", text_color="orange")],
        [
            sg.Text("Select Dataset", font=15),
            sg.Input(size=(40, 4), disabled=True, enable_events=True, text_color="black", key="-DATASET-"),
            sg.FolderBrowse()
        ],
        [sg.Button("Ok", disabled=True), sg.Button("Cancel")],
    ]

    layout = [[sg.Column(column_layout, element_justification="center")]]

    dataset_window = sg.Window(title="Dataset selection",
                               layout=layout,
                               finalize=True)
    while True:
        event, value = dataset_window.read()
        if event == "Cancel" or event == sg.WIN_CLOSED:
            data = None
            break
        # check input not empty
        if event == '-DATASET-' and value["-DATASET-"] != '':
            error = insert_dataset(value["-DATASET-"])
            if error is not None:
                data = None
                dataset_window["Ok"].update(disabled=True)
                sg.Popup(error)
            else:
                data = value["-DATASET-"]
                dataset_window["Ok"].update(disabled=False)

        if event == "Ok":
            break

    dataset_window.close()
    return data


def show_labels_layout(dataset):
    """ Handle the addition of a new label file.

        Parameters
        ----------
        dataset: str
            The string representing the path of the dataset

        Returns
        -------
        labels: Dataframe
            The labels in a pandas Dataframe object, if no error occurs
        """
    labels = None
    # Create a pop up window
    label_layout = [
        [sg.Text("Upload the label file.", text_color="orange")],
        [sg.Text("Format must be CSV.", text_color="orange")],
        [
            sg.Text("Select the file", font=15),
            sg.Input(disabled=True, enable_events=True, text_color="black", key="-LABELS-"),
            sg.FileBrowse(file_types=(("CSV Files", "*.csv"),), key="-IN-")
        ],
        [sg.Button("Submit", disabled=True, key="-SUBMIT_LABELS-"), sg.Button("Cancel")]
    ]

    layout = [[sg.Column(label_layout, element_justification="center")]]

    label_window = sg.Window(title="Insert labels",
                             layout=layout)
    while True:
        event, values = label_window.read()
        if event == "Cancel" or event == sg.WIN_CLOSED or event == "Submit":
            break
        elif event == "-LABELS-" and values["-LABELS-"] != "":
            label_window["-SUBMIT_LABELS-"].update(disabled=False)
        elif event == "-SUBMIT_LABELS-":
            labels, error = insert_labels(values["-IN-"], dataset)
            if error is not None:
                sg.Popup(error)
                label_window["-SUBMIT_LABELS-"].update(disabled=True)
            break
    label_window.close()
    return labels


def split_and_renovation(dataset, labels: pd.DataFrame):
    """ Handle the split of the dataset.

            Parameters
            ----------
            dataset: str
                The string representing the path of the dataset
            labels: Dataframe
                The labels in a pandas Dataframe object

            Returns
            -------
            split: float
                The amount of dataset to keep as training (expressed as float)
            """
    split = 0.7
    renovation_layout = [
        [sg.Column([[sg.Text("The input folder of the dataset will be restructured", text_color="orange")],
                    [sg.Text("A number of subfolders equal to the number of labels will be created",
                             text_color="orange")],
                    [sg.Text("Each folder will contain the images related to the label", text_color="orange")]],
                   justification='left'),
         sg.Column([[sg.Button(image_filename="./icons/play.png", size=(10, 5), key="-PLAY-")]], justification='right')]
    ]
    column_layout = [
        [sg.Text("Select split value")],
        [sg.Slider(range=(1, 100), default_value=70, size=(70, 20), orientation="h", enable_events=True,
                   key="-SPLIT-")],
        [sg.VPush()], [sg.Column(renovation_layout)], [sg.VPush()],
        [sg.Button("OK"), sg.Button("Cancel")]
    ]

    layout = [[sg.Column(column_layout, element_justification="center")]]
    split_window = sg.Window(title="Split",
                             layout=layout)
    is_renovate = False
    while True:
        event, values = split_window.read()
        if event == "Cancel" or event == sg.WIN_CLOSED or event == "Submit":
            break
        if event == "-SPLIT-":
            split = values["-SPLIT-"] / 100
        if event == "-PLAY-":
            error = dataset_renovation(dataset, labels)
            if error is None:
                is_renovate = True
                sg.popup("Successful restructuring")
            else:
                sg.popup(error)
                is_renovate = False
        if event == "OK":
            if not is_renovate:
                sg.popup("The dataset has not yet been restructured,"
                         "recommend renovation if not already done manually")
            break

    split_window.close()
    return split


def choose_model(models, optimizers, losses):
    """ UI section that let the user select which model to run and the hyperparameters.

        Parameters
        ----------
        models: set
            Set of available models
        optimizers: set
            Set of available optimizers
        losses: set
            Set of available losses

        Returns
        ----------
        set
            Set of selected hyperparameters: model, epochs, batch_size, optimizer, lr, loss, dropout
        """
    model, epochs, batch_size, optimizer, lr, loss = None, None, None, None, None, None
    dropout = False

    # Model selection
    model_layout = [
        [sg.Text("Choose a model to train on.", text_color="orange")],
        [
            sg.Text("Select the model", font='Any 10'),
            sg.Combo(models, default_value=models[0], readonly=True, key='-COMBO-')
        ]
    ]
    # Epochs selection
    epochs_layout = [
        [sg.Text("Choose for how many epochs train the model.", text_color="orange")],
        [
            sg.Text("Choose the epochs value", font='Any 10'),
            sg.InputText(size=(10, 1), default_text='20', key='-EPOCHS-')
        ]
    ]
    # Batch_size selection
    batch_size_layout = [
        [sg.Text("Choose the batch size for training the model (a multiple of 2 is preferable).", text_color="orange")],
        [
            sg.Text("Choose the batch size", font='Any 10'),
            sg.InputText(size=(10, 1), default_text='16', key='-BATCH-')
        ]
    ]
    # optimizer selection
    optimizer_layout = [
        [sg.Text("Choose the optimizer for the training.", text_color="orange")],
        [
            sg.Text("Select the optimizer", font='Any 10'),
            sg.Combo(optimizers, default_value=optimizers[0], readonly=True, key='-COMBO_OPT-')
        ]
    ]
    # lr selection
    lr_layout = [
        [sg.Text("Choose the learning rate for the optimizer.", text_color="orange")],
        [
            sg.Text("Choose the learning rate", font='Any 10'),
            sg.InputText(size=(10, 1), default_text='0.0001', key='-LR-')
        ]
    ]
    # loss selection
    loss_layout = [
        [sg.Text("Choose the loss for the training.", text_color="orange")],
        [
            sg.Text("Select the loss", font='Any 10'),
            sg.Combo(losses, default_value=losses[0], readonly=True, key='-COMBO_L-')
        ]
    ]
    # dropout selection
    dropout_layout = [
        [sg.Text("Choose whether to apply Dropout during training.", text_color="orange")],
        [
            sg.Text("Check if you want to apply Dropout", font='Any 10'),
            sg.Checkbox("", key='-DROP-')
        ]
    ]

    layout = [
        [sg.Column(model_layout, element_justification="left")],
        [sg.Column(epochs_layout, element_justification="left")],
        [sg.Column(batch_size_layout, element_justification="left")],
        [sg.Column(optimizer_layout, element_justification="left")],
        [sg.Column(lr_layout, element_justification="left")],
        [sg.Column(loss_layout, element_justification="left")],
        [sg.Column(dropout_layout, element_justification="left")],
        [sg.Column([[sg.Button("Submit", disabled=False, key="-SUBMIT_MODEL-"), sg.Button("Cancel")]],
                   element_justification="center")]
    ]

    model_window = sg.Window(title="Choose a model",
                             layout=layout)

    while True:
        event, values = model_window.read()
        if event == "Cancel" or event == sg.WIN_CLOSED:
            break
        elif event == "-SUBMIT_MODEL-":
            if values["-EPOCHS-"] == '' or values["-BATCH-"] == '' or values["-LR-"] == '':
                sg.PopupError("Missing some values.")
                continue
            else:
                try:
                    epochs = int(values["-EPOCHS-"])
                    batch_size = int(values["-BATCH-"])
                except Exception as e:
                    sg.PopupError("You should type an integer.")
                    continue
                try:
                    lr = float(values["-LR-"])
                    if lr < 0 or lr > 1:
                        sg.PopupError("Learning rate should be between 0 and 1.")
                        continue
                except Exception as e:
                    sg.PopupError("You should type a float.")
                    continue

            model = values["-COMBO-"]
            optimizer = values["-COMBO_OPT-"]
            loss = values["-COMBO_L-"]
            dropout = values["-DROP-"]
            break

    model_window.close()

    return model, epochs, batch_size, optimizer, lr, loss, dropout


def draw_figure_w_toolbar(canvas, fig, canvas_toolbar):
    """ Draw the plot and toolbar.

        Parameters
        ----------
        canvas: sg.Canvas
            The canvas containing the plots
        fig: Any
            The plot's figure
        canvas_toolbar: sg.Canvas
            The canvas containing the toolbar

        Returns
        ----------
        figure_canvas_agg: FigureCanvasTkAgg
            The canvas containing plots and toolbar
        """
    if canvas.children:
        for child in canvas.winfo_children():
            child.destroy()
    if canvas_toolbar.children:
        for child in canvas_toolbar.winfo_children():
            child.destroy()
    figure_canvas_agg = FigureCanvasTkAgg(fig, master=canvas)
    figure_canvas_agg.draw()
    toolbar = NavigationToolbar2Tk(figure_canvas_agg, canvas_toolbar)
    toolbar.update()
    figure_canvas_agg.get_tk_widget().pack(side='right', fill='both', expand=1)
    return figure_canvas_agg


def delete_figure_agg(figure_agg, canvas_toolbar):
    """ Delete plot and toolbar.

        Parameters
        ----------
        figure_agg: Any
            The plot's figure
        canvas_toolbar: sg.Canvas
            The canvas containing the toolbar

        """
    if canvas_toolbar.children:
        for child in canvas_toolbar.winfo_children():
            child.destroy()
    figure_agg.get_tk_widget().forget()
    plt.close('all')
