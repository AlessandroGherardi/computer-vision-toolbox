import pandas as pd
import filetype
import os
import shutil


def get_size(folder):
    """ Calculates the size of the input folder

    Parameters
    ----------
    folder: str
        The path corresponding to the dataset folder

    Returns
    ----------
    size: int
        Size of the folder
    """
    size = 0
    for element in os.scandir(folder):
        size += os.path.getsize(element)
    return size


def check_folder(folder):
    """ Check if the folder only contains images

    Parameters
    ----------
    folder: str
        The path corresponding to the dataset folder

    Returns
    ----------
    bool
        True if folder only contains images, False otherwise
    """
    if any(os.path.isdir(folder + "/" + file) for file in os.listdir(folder)):
        return False
    elif all(filetype.is_image(folder + "/" + File) for File in os.listdir(folder)):
        return True
    else:
        return False


def check_image_type(folder):
    """ Check if the folder only contains images with the specified extension

    Parameters
    ----------
    folder: str
        The path corresponding to the dataset folder

    Returns
    ----------
    bool
        True if the folder only contains images with PNG, BMP or JPG(JPEG) extension, False otherwise
    """
    if all(File.endswith(".jpeg") or File.endswith(".png") or File.endswith(".bmp") or File.endswith(".jpg")
           for File in os.listdir(folder)):
        return True
    else:
        return False


def move(dataset, labels: pd.DataFrame):
    """ Move images to the folder of the respective label

    Parameters
    ----------
    dataset: str
        The path corresponding to the dataset folder
    labels: pandas DataFrame
        Labels in a pandas DataFrame object

    """
    for i in range(0, len(labels.index)):
        source_path = dataset + "/" + str(labels.loc[i, 0])
        image_class = labels.loc[i, 1]
        move_directory = dataset + "/" + str(image_class)
        shutil.move(source_path, move_directory)


def insert_dataset(folder):
    """ Check if the folder contains only images, in the correct format and of the right size

    Parameters
    ----------
    folder: str
        The path corresponding to the dataset folder

    Returns
    ----------
    str
        A string that will contain the error if it occurs, None if there are no errors
    """
    # controllo che tutti i file siano immagini
    if not check_folder(folder):
        return "Error: folder does not contains only images"
    # controllo che il formato delle immagini sia corretto
    elif not check_image_type(folder):
        return "Error: folder does not contains only image of the specified types"
    # controllo dimensione dei dati non > 500MB
    elif get_size(folder) >= 500 * 1024 * 1024:
        return "Error: Correct format, but the folder is too big"
    else:
        return None


def insert_labels(values, dataset):
    """ Reads a CSV file containing the labels that match to the dataset

    Parameters
    ----------
    values: str
        The path corresponding to the CSV file
    dataset: str
        The path corresponding to the dataset folder

    Returns
    ----------
    labels: Dataframe
        The labels in a pandas Dataframe object
    error: str
        A string that will contain the error if it occurs
    """
    labels, error = None, None
    # Check file
    try:
        labels = pd.read_csv(values, sep=',', header=None)
        # Parse file
        if labels.shape[1] != 2:
            error = "Error: Incorrectly formatted file."
        else:
            try:
                # Check the labels are integers
                labels.iloc[:, 1].astype(int)
                # Check files path
                for image in labels.iloc[:, 0]:
                    if not os.path.isfile(os.path.join(dataset + '/', image)):
                        error = "Error: cannot found image " + image
                        break
                # Check consistency with images
                images = set([os.path.join(dataset + '/', image) for image in os.listdir(dataset)])
                test = set([os.path.join(dataset + '/', image) for image in labels.iloc[:, 0]]).difference(images)
                if len(test) > 0:
                    labels = None
                    error = "Error: lack of consistency betweeen dataset and labels."
                return labels, error
            except Exception as e:
                labels = None
                error = "Error: " + str(e)

            return labels, error
    except Exception as e:
        labels = None
        error = "Error: cannot open this csv file."

    return labels, error


def dataset_renovation(dataset, labels: pd.DataFrame):
    """ Check if the dataset is already restructured and if it's not, create subfolders related to the labels

    Parameters
    ----------
    dataset: str
        The path corresponding to the dataset folder
    labels: DataFrame
        Labels in a pandas DataFrame object

    Returns
    -------
    str
        A string that will contain the error if it occurs, None if there are no errors
    """
    if not check_folder(dataset):
        return "The dataset has already been restructured or does not contain only images"
    else:
        folders = set(labels.loc[:, 1])
        for i in folders:
            new_dir = dataset + "/" + str(i)
            if not os.path.exists(new_dir):
                os.makedirs(new_dir)
        move(dataset, labels)
        return None
