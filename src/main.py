import PySimpleGUI as sg
from .frontend import *
from .backend import *


def main():
    figure_agg = None
    CURRENT_MODEL = None
    CURRENT_MODEL_RESULTS = None

    # Set containing implemented models
    MODELS = ("MobileNet-v2", "ResNet-101")
    # Set containing implemented optimizers
    OPTIMIZERS = ("Adam", "SGD", "RMSprop")
    # Set containing implemented losses
    LOSSES = ("sparse_categorical_crossentropy", "binary_crossentropy", "categorical_crossentropy")
    # Dictionary containing all selected parameters
    PARAMETERS = {}
    # Dictionary containig models params
    MODEL_PAR = {}
    model_to_saved = 1
    # Define theme
    sg.theme("DarkGrey")

    # Define layouts
    parameter_selection_layout = [
        [
            sg.Column([]),  # clear session
            sg.Column([
                [sg.Text("Insert Dataset")],
                [sg.Button(image_filename="./icons/dataset.png", key="-DATASET_INSERT-")]],
                element_justification="center", expand_x=True),  # select dataset
            sg.Column([  # select labels
                [sg.Text("Insert labels")],
                [sg.Button(image_filename="./icons/labels.png", key='-SELECT_LABELS-')]
            ], element_justification="center", expand_x=True),
            sg.Column([
                [sg.Text('Split')],
                [sg.Button(image_filename="./icons/split.png", key='-SPLIT-')]],
                element_justification="center", expand_x=True),  # split selection
            sg.Column([  # model selection
                [sg.Text("Choose a model")],
                [sg.Button(image_filename="./icons/model.png", key='-SELECT_MODEL-')]
            ], element_justification="center", expand_x=True),
            sg.Column([  # train
                [sg.Text("Train")],
                [sg.Button(image_filename="./icons/play.png", key='-TRAIN-')]
            ], element_justification="center", expand_x=True),
        ]
    ]
    chart_selection_layout = [
        [
            sg.Column([  # chart acc/loss
                [sg.Text("Accuracy/Loss")],
                [sg.Button(image_filename="./icons/chart.png", key='-CHART-PLOT-')]
            ], element_justification="center", expand_x=True),
            sg.Column([  # chart cm
                [sg.Text("Confusion matrix")],
                [sg.Button(image_filename="./icons/cm.png", key='-CM-PLOT-')]
            ], element_justification="center", expand_x=True),
        ]
    ]
    graphical_feedback = [
        [sg.Text("DATASET INFO")],
        [sg.Text("Path: "), sg.Text("", key="-PATHDATA-")],
        [sg.Text("# images: "), sg.Text("", key="-NUMIMG-")],
        [sg.Text("Dimension: "), sg.Text("", key="-DIMDATA-")],
        [sg.Text("LABELS INFO")],
        [sg.Text("Labels: "), sg.Text("", key="-LABELS-")],
        [sg.Text("SPLIT INFO")],
        [sg.Text("Split: "), sg.Text("", key="-SPLITVALUE-")],
        [sg.Text("MODEL INFO")],
        [sg.Text("Model loaded: "), sg.Text("", key="-MODEL-")],
        [sg.Text("Epoch: "), sg.Text("", key="-EPOCH-")],
        [sg.Text("Batch size: "), sg.Text("", key="-BATCH-")],
        [sg.Text("Optimizer: "), sg.Text("", key="-OPTIMIZER-")],
        [sg.Text("Learning rate: "), sg.Text("", key="-LEARNINGRATE-")],
        [sg.Text("Loss: "), sg.Text("", key="-LOSS-")],
        [sg.Text("Dropout: "), sg.Text("", key="-DROPOUT-")],
    ]

    output_graphical_feedback = [
        [sg.Text("Model: "), sg.Text("", key="-MMODEL-")],
        [sg.Text("Epoch: "), sg.Text("", key="-MEPOCH-")],
        [sg.Text("Batch size: "), sg.Text("", key="-MBATCH-")],
        [sg.Text("Optimizer: "), sg.Text("", key="-MOPTIMIZER-")],
        [sg.Text("Learning rate: "), sg.Text("", key="-MLEARNINGRATE-")],
        [sg.Text("Loss: "), sg.Text("", key="-MLOSSFUN-")],
        [sg.Text("Dropout: "), sg.Text("", key="-MDROPOUT-")],
        [sg.Text("Train Accuracy: "), sg.Text("", key="-MTACCURACY-")],
        [sg.Text("Train Loss: "), sg.Text("", key="-MTLOSS-")],
        [sg.Text("Val Accuracy: "), sg.Text("", key="-MVACCURACY-")],
        [sg.Text("Val Loss: "), sg.Text("", key="-MVLOSS-")],
    ]

    models_history_layout = []
    results_layout = [
        [sg.Column(output_graphical_feedback)]
    ]
    chart_layout = [
        [sg.Column([
            [sg.Canvas(key='-CONTROLS-')],
            [sg.Column(
                layout=[
                    [sg.Canvas(key='-FIGURE-',
                               size=(1000, 1000)
                               )]
                ]
            )],
            [sg.Button('Clear', key="-CLEAR-CHART-", visible=False)]
        ])]
    ]

    parameters_frame = sg.Frame("Parameters selection", parameter_selection_layout, expand_x=True)
    chart_selection_frame = sg.Frame("Charts selection", chart_selection_layout, expand_x=True)
    output_chart_layout = [
        sg.Frame("output", layout=results_layout, visible=False, key="-output_val-"),
        sg.Frame("", layout=chart_layout, expand_y=True, expand_x=True, element_justification='center')
    ]
    output_chart_frame = sg.Frame("Results", [output_chart_layout], expand_y=True, expand_x=True)

    main_layout = [
        [
            parameters_frame,
            chart_selection_frame
        ],
        [sg.Column([
            [sg.Column([[sg.Frame("Info", size=(475, 440), layout=graphical_feedback)]])],
            [sg.Column([[sg.Frame("Model History", size=(475, 400), layout=models_history_layout, key='-MODEL_HIST-')]],
                       scrollable=True, vertical_scroll_only=True, size=(500, 400))]
        ]),
            output_chart_frame
        ]]

    # Define main window
    main_window = sg.Window(title='Computer Vision Toolbox',
                            layout=main_layout,
                            location=(0, 0),
                            resizable=True,
                            finalize=True)
    main_window.maximize()

    # Event loop
    while True:
        event, values = main_window.read()
        # Capture signals
        if event == sg.WIN_CLOSED:
            break

        if event == "-DATASET_INSERT-":
            main_window.disable()
            dataset = dataset_in()
            PARAMETERS["dataset"] = dataset
            main_window.enable()
            main_window.BringToFront()
            if PARAMETERS["dataset"] is not None:
                main_window["-PATHDATA-"].update(PARAMETERS["dataset"])
                main_window["-NUMIMG-"].update(len(os.listdir(PARAMETERS["dataset"])))
                main_window["-DIMDATA-"].update("{:.2f} MB".format(get_size(PARAMETERS["dataset"]) / 1024 / 1024))
            else:
                main_window["-PATHDATA-"].update()
                main_window["-NUMIMG-"].update()
                main_window["-DIMDATA-"].update()

        if event == "-SELECT_LABELS-":
            if "dataset" in PARAMETERS and PARAMETERS["dataset"] is not None and PARAMETERS["dataset"] != "":
                main_window.disable()
                PARAMETERS["labels"] = show_labels_layout(PARAMETERS["dataset"])
                main_window.enable()
                main_window.BringToFront()
                if PARAMETERS["labels"] is not None:
                    lab = set(PARAMETERS["labels"].iloc[:, 1])
                    main_window["-LABELS-"].update(lab)
                else:
                    main_window["-LABELS-"].update()
            else:
                sg.Popup("You should select a dataset before.")

        if event == "-SPLIT-":
            if "dataset" not in PARAMETERS or PARAMETERS["dataset"] is None or PARAMETERS["dataset"] == "":
                sg.Popup("You should select a dataset before.")
            elif "labels" not in PARAMETERS or PARAMETERS["labels"] is None:
                sg.Popup("You should select a labels file before.")
            else:
                main_window.disable()
                PARAMETERS["split"] = split_and_renovation(PARAMETERS["dataset"], PARAMETERS["labels"])
                main_window.enable()
                main_window.BringToFront()
                if PARAMETERS["split"] is not None:
                    main_window["-SPLITVALUE-"].update("{:.1f}% Dataset / {:.1f}% Validation Set".format(
                        PARAMETERS["split"] * 100, 100 - PARAMETERS["split"] * 100))
                else:
                    main_window["-SPLITVALUE-"].update()

        if event == "-SELECT_MODEL-":
            main_window.disable()
            PARAMETERS["model_selected"], \
            PARAMETERS["epochs_selected"], \
            PARAMETERS["batch_size_selected"], \
            PARAMETERS["optimizer_selected"], \
            PARAMETERS["lr_selected"], \
            PARAMETERS["loss_selected"], \
            PARAMETERS["dropout_selected"] = choose_model(MODELS, OPTIMIZERS, LOSSES)
            main_window.bring_to_front()
            if PARAMETERS["model_selected"] is not None:
                PARAMETERS["model_loaded"] = instantiate_model(PARAMETERS["model_selected"])
            elif "model_loaded" not in PARAMETERS or PARAMETERS["model_selected"] is None:
                sg.Popup("Cannot load the model, select at least one.")
            main_window.bring_to_front()
            main_window.enable()
            if PARAMETERS["model_selected"] is not None:
                main_window["-MODEL-"].update(PARAMETERS["model_selected"])
                main_window["-EPOCH-"].update(PARAMETERS["epochs_selected"])
                main_window["-BATCH-"].update(PARAMETERS["batch_size_selected"])
                main_window["-OPTIMIZER-"].update(PARAMETERS["optimizer_selected"])
                main_window["-LEARNINGRATE-"].update(PARAMETERS["lr_selected"])
                main_window["-LOSS-"].update(PARAMETERS["loss_selected"])
                main_window["-DROPOUT-"].update(PARAMETERS["dropout_selected"])
            else:
                main_window["-MODEL-"].update()
                main_window["-EPOCH-"].update()
                main_window["-BATCH-"].update()
                main_window["-OPTIMIZER-"].update()
                main_window["-LEARNINGRATE-"].update()
                main_window["-LOSS-"].update()
                main_window["-DROPOUT-"].update()

        if event == "-TRAIN-":
            if "model_loaded" not in PARAMETERS or PARAMETERS["model_loaded"] is None:
                sg.popup("You should choose a model before.")
            elif "dataset" not in PARAMETERS or PARAMETERS["dataset"] is None or PARAMETERS["dataset"] == "":
                sg.Popup("You should select a dataset before.")
            elif "labels" not in PARAMETERS or PARAMETERS["labels"] is None:
                sg.Popup("You should select a labels file before.")
            elif "split" not in PARAMETERS or PARAMETERS["split"] is None:
                sg.Popup("You should split your dataset before.")
            elif "epochs_selected" not in PARAMETERS or PARAMETERS["epochs_selected"] is None:
                sg.Popup("You should choose the hyperparameters before.")
            else:
                current_model, history, train_acc, train_loss, val_acc, val_loss = train_eval_model(
                    PARAMETERS["model_selected"],
                    PARAMETERS["model_loaded"],
                    PARAMETERS["dataset"],
                    PARAMETERS["split"],
                    PARAMETERS["epochs_selected"],
                    PARAMETERS["batch_size_selected"],
                    PARAMETERS["optimizer_selected"],
                    PARAMETERS["lr_selected"],
                    PARAMETERS["loss_selected"],
                    PARAMETERS["dropout_selected"]
                    )
                if current_model is None and history is None and train_acc is None and train_loss is None and val_acc is None and val_loss is None:
                    sg.Popup("Error during training, maybe you have choose the wrong parameters...")
                else:

                    param_name = "model" + str(model_to_saved)
                    res_name = "model" + str(model_to_saved) + "_result"
                    model_name = "model" + str(model_to_saved) + "_model"
                    if model_to_saved == 1:
                        MODEL_PAR["model1"] = PARAMETERS.copy()
                        MODEL_PAR["model1_result"] = [train_acc, train_loss, val_acc, val_loss, history]
                        MODEL_PAR["model1_model"] = current_model
                        main_window.extend_layout(main_window['-MODEL_HIST-'], add_history_layout(model_to_saved))
                        name = "{} -- ACC: {}% -- LOSS: {}".format(PARAMETERS["model_selected"], val_acc, val_loss)
                        params = "SPLIT: {} -- EPOCHS: {} -- OPT: {} -- LOSS: {}".format(PARAMETERS["split"],
                                                                                         PARAMETERS["epochs_selected"],
                                                                                         PARAMETERS["optimizer_selected"],
                                                                                         PARAMETERS["lr_selected"])
                        main_window["-LOAD1-"].update(disabled=False)
                        main_window["-MODEL1NAME-"].update(name)
                        main_window["-MODEL1PARAM-"].update(params)
                        model_to_saved = model_to_saved + 1
                    elif check_if_model_already_saved(MODEL_PAR, PARAMETERS, model_to_saved):
                        if model_to_saved > 4:
                            new_height = 100 * model_to_saved
                            main_window["-MODEL_HIST-"].set_size(size=(475, new_height))
                        MODEL_PAR[param_name] = PARAMETERS.copy()
                        MODEL_PAR[res_name] = [train_acc, train_loss, val_acc, val_loss, history]
                        MODEL_PAR[model_name] = current_model
                        main_window.extend_layout(main_window['-MODEL_HIST-'], add_history_layout(model_to_saved))
                        name = "{} -- VAL ACC: {}% -- VAL LOSS: {}".format(PARAMETERS["model_selected"], val_acc, val_loss)
                        params = "SPLIT: {} -- EPOCHS: {} -- OPT: {} -- LOSS: {}".format(PARAMETERS["split"],
                                                                                         PARAMETERS["epochs_selected"],
                                                                                         PARAMETERS["optimizer_selected"],
                                                                                         PARAMETERS["lr_selected"])
                        load_key = "-LOAD" + str(model_to_saved) + "-"
                        model_key = "-MODEL" + str(model_to_saved) + "NAME-"
                        param_key = "-MODEL" + str(model_to_saved) + "PARAM-"
                        main_window[load_key].update(disabled=False)
                        main_window[model_key].update(name)
                        main_window[param_key].update(params)
                        model_to_saved = model_to_saved + 1

        if event == "-CHART-PLOT-":
            if CURRENT_MODEL_RESULTS is not None:
                if figure_agg:
                    # Clean up previous drawing before drawing again
                    delete_figure_agg(figure_agg, main_window['-CONTROLS-'].TKCanvas)
                    main_window["-CLEAR-CHART-"].update(visible=False)
                fig = plot_history(CURRENT_MODEL_RESULTS[4])
                _, _, w, h = fig.bbox.bounds
                main_window['-FIGURE-'].set_size(size=(w, h))
                figure_agg = draw_figure_w_toolbar(main_window['-FIGURE-'].TKCanvas, fig,
                                                   main_window['-CONTROLS-'].TKCanvas)
                main_window["-CLEAR-CHART-"].update(visible=True)
            else:
                sg.PopupError("Please load a model...")

        if event == "-CM-PLOT-":
            if CURRENT_MODEL is not None:
                if figure_agg:
                    # Clean up previous drawing before drawing again
                    delete_figure_agg(figure_agg, main_window['-CONTROLS-'].TKCanvas)
                    main_window["-CLEAR-CHART-"].update(visible=False)
                fig = plot_cm(CURRENT_MODEL)
                _, _, w, h = fig.bbox.bounds
                main_window['-FIGURE-'].set_size(size=(w, h))
                figure_agg = draw_figure_w_toolbar(main_window['-FIGURE-'].TKCanvas, fig,
                                                   main_window['-CONTROLS-'].TKCanvas)
                main_window["-CLEAR-CHART-"].update(visible=True)
            else:
                sg.PopupError("Please load a model...")

        if event == "-CLEAR-CHART-" and figure_agg:
            delete_figure_agg(figure_agg, main_window['-CONTROLS-'].TKCanvas)
            main_window["-CLEAR-CHART-"].update(visible=False)

        for i in range(1, model_to_saved):
            load_button = "-LOAD" + str(i) + "-"
            if event == load_button:
                main_window["-output_val-"].update(visible=True)
                name = "model" + str(i)
                result = "model" + str(i) + "_result"
                model_name = "model" + str(i) + "_model"
                if figure_agg:
                    # Clean up previous drawing before drawing again
                    delete_figure_agg(figure_agg, main_window['-CONTROLS-'].TKCanvas)
                    main_window["-CLEAR-CHART-"].update(visible=False)
                model_param = MODEL_PAR[name]
                model_result = MODEL_PAR[result]
                CURRENT_MODEL_RESULTS = MODEL_PAR[result]
                CURRENT_MODEL = MODEL_PAR[model_name]
                main_window["-MMODEL-"].update(model_param["model_selected"])
                main_window["-MEPOCH-"].update(model_param["epochs_selected"])
                main_window["-MBATCH-"].update(model_param["batch_size_selected"])
                main_window["-MOPTIMIZER-"].update(model_param["optimizer_selected"])
                main_window["-MLEARNINGRATE-"].update(model_param["lr_selected"])
                main_window["-MLOSSFUN-"].update(model_param["loss_selected"])
                main_window["-MDROPOUT-"].update(model_param["dropout_selected"])
                main_window["-MTACCURACY-"].update(model_result[0])
                main_window["-MTLOSS-"].update(model_result[1])
                main_window["-MVACCURACY-"].update(model_result[2])
                main_window["-MVLOSS-"].update(model_result[3])

    # Close main window
    main_window.close()
