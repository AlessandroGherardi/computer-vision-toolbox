import os
import shutil
import unittest
from ..frontend import *
from ..backend import *


class TestToolbox(unittest.TestCase):
    """ TestToolbox is a class that manages the unit test of the main functions implemented for the Computer-Vision
    Toolbox.
    """

    def test_insert_dataset(self):
        """ Test the addition of a new dataset.
            """
        path_jpg = r'./src/unit_test/test_loading_dataset/jpeg'
        path_png = r'./src/unit_test/test_loading_dataset/png'
        path_bmp = r'./src/unit_test/test_loading_dataset/bmp'
        path_err_tiff = r'./src/unit_test/test_loading_dataset/tiff_error'
        path_err_sub_dir = r'./src/unit_test/test_loading_dataset/sub_directory_error'
        path_err_txt = r'./src/unit_test/test_loading_dataset/file_txt_error'

        self.assertIsNone(insert_dataset(path_jpg))
        self.assertIsNone(insert_dataset(path_png))
        self.assertIsNone(insert_dataset(path_bmp))
        self.assertIsNotNone(insert_dataset(path_err_tiff))
        self.assertIsNotNone(insert_dataset(path_err_sub_dir))
        self.assertIsNotNone(insert_dataset(path_err_txt))


    def test_insert_labels(self):
        """ Test the addition of a label file.
            """
        dataset_path = r'./src/unit_test/test_loading_labels/images'
        labels_path = r'./src/unit_test/test_loading_labels/labels.csv'
        test_labels, test_error = insert_labels(labels_path, dataset_path)
        self.assertIsNone(test_error)
        self.assertIsNotNone(test_labels)

        labels_path = 2
        test_labels, test_error = insert_labels(labels_path, dataset_path)
        self.assertIsNotNone(test_error)
        self.assertIsNone(test_labels)

        labels_path = r'./src/unit_test/test_loading_labels/labels_error.csv'
        test_labels, test_error = insert_labels(labels_path, dataset_path)
        self.assertIsNotNone(test_error)
        self.assertIsNone(test_labels)

        labels_path = r'./src/unit_test/test_loading_labels/labels_error2.csv.csv'
        test_labels, test_error = insert_labels(labels_path, dataset_path)
        self.assertIsNotNone(test_error)
        self.assertIsNone(test_labels)

        labels_path = r'./src/unit_test/test_loading_labels/labels_error3.csv.csv'
        test_labels, test_error = insert_labels(labels_path, dataset_path)
        self.assertIsNotNone(test_error)
        self.assertIsNone(test_labels)

    def test_renovation(self):
        """ Test the dataset split.
            """
        dataset_path = r'./src/unit_test/test_split/images'
        new_folder_path = r'./src/unit_test/test_split/test'
        labels_path = r'./src/unit_test/test_split/labels.csv'
        labels = pd.read_csv(labels_path, sep=',', header=None)

        # new folder creation with images
        if os.path.exists(new_folder_path):
            shutil.rmtree(new_folder_path)
        shutil.copytree(dataset_path, new_folder_path)

        # renovation
        dataset_renovation(new_folder_path, labels)

        # check that every image is in the right directory
        for i in range(0,len(labels.index)):
            # extract file name
            file = labels.loc[i, 0]
            # extract label
            renovate_dir = str(labels.loc[i, 1])
            # create path with folder + renovate folder with label name + file
            path = new_folder_path + "/" + renovate_dir + "/" + file
            # assert the path exists
            self.assertTrue(os.path.exists(path))

    def test_check_parameters(self):
        """ Test if a model is already been saved.
            """
        param = {'dataset': "fake/path", 'split': 0.7, 'model_selected': "mobile_net", 'epochs_selected': 5,
                 'lr_selected': 0.0001, 'dropout_selected': False, 'model_loaded': 'A'}
        param1 = {'dataset': "fake/path", 'split': 0.5, 'model_selected': "mobile_net", 'epochs_selected': 8,
                  'lr_selected': 0.0001, 'dropout_selected': False, 'model_loaded': 'A'}
        param2 = {'dataset': "fake/path", 'split': 0.7, 'model_selected': "mobile_net", 'epochs_selected': 5,
                  'lr_selected': 0.0001, 'dropout_selected': False, 'model_loaded': 'B'}
        param3 = {'dataset': "fake/path/new", 'split': 0.7, 'model_selected': "mobile_net", 'epochs_selected': 5,
                  'lr_selected': 0.0005, 'dropout_selected': True, 'model_loaded': 'A'}
        model_ok = {}
        model_err = {}
        model_err_load = {}

        model_err_load['model1'] = param1.copy()
        model_err_load['model2'] = param2.copy()
        model_err_load['model3'] = param3.copy()
        model_err['model1'] = param1.copy()
        model_err['model2'] = param2.copy()
        model_ok['model1'] = param1.copy()
        model_ok['model2'] = param3.copy()

        self.assertFalse(check_if_model_already_saved(model_err_load, param, 4))
        self.assertFalse(check_if_model_already_saved(model_err, param, 3))
        self.assertTrue(check_if_model_already_saved(model_ok, param, 3))


    def test_load_model(self):
        """ Test the model loading.
            """
        model = "MobileNet-v2"
        test_model = instantiate_model(model)
        self.assertIsNotNone(test_model)

        model = "ResNet-101"
        test_model = instantiate_model(model)
        self.assertIsNotNone(test_model)

        model = "ResNet-Error"
        test_model = instantiate_model(model)
        self.assertIsNone(test_model)


    def test_train_eval_model(self):
        """ Test the model training.
            """
        model_name = "MobileNet-v2"
        epochs = 10
        model = tf.keras.models.load_model('./models/' + model_name)
        dataset = "./src/unit_test/test_training/"
        train_split = 0.7
        epochs = 2
        batch = 8
        opt = "Adam"
        lr = 0.001
        loss = "sparse_categorical_crossentropy"
        dropout = True
        results = train_eval_model(model_name, model, dataset, train_split, epochs, batch, opt, lr, loss, dropout)
        self.assertIsNotNone(results)


if __name__ == '__main__':
    unittest.main()
